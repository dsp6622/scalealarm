#!/usr/bin/python3
import RPi.GPIO as GPIO
import os
from time import sleep
import pygame.mixer as mix

os.chdir(os.path.dirname(os.path.realpath(__file__)))

ALARM_FILE_NAME = 'alarm.wav'
RELAX_FILE_NAME = 'relax.wav'

SAMPLE_FREQ = 44100 
BIT_SIZE = -16
CHANNELS = 2
BUFFER_SIZE = 2048

GPIO_PIN = 40
ALARM_VOLUME = 1 #(between 0 and 1)
RELAX_VOLUME = .7

GPIO.setmode(GPIO.BOARD)
GPIO.setup(GPIO_PIN, GPIO.IN)

# Initialize pygame.mixer
mix.init(SAMPLE_FREQ, BIT_SIZE, CHANNELS, BUFFER_SIZE)
mix.init()

# Load wav files into Sound objects
alarm = mix.Sound(ALARM_FILE_NAME)
relax = mix.Sound(RELAX_FILE_NAME)

# Channels allow us to play more than one thing at the same time. 
# Here I'm just using them so I can easily pause and resume
alarmChannel = mix.Channel(1)
relaxChannel = mix.Channel(2)

alarm.set_volume(ALARM_VOLUME)
relax.set_volume(RELAX_VOLUME)

print('Files read\n')
# Play alarm track n+1 times. If you want this to be infinite, use -1
alarmChannel.play(alarm,4)
relaxChannel.queue(relax)
relaxChannel.pause()

switch=True
switchLast=True

# Loop until the relax track has finished (or alarm channel if not infinite)
while relaxChannel.get_sound() != None and alarmChannel.get_sound() != None:

    if switch != switchLast:
        switchLast = switch
        if not switch:
            alarmChannel.pause()
            relaxChannel.unpause()
        else:
            relaxChannel.pause()
            alarmChannel.unpause()
    switch = GPIO.input(GPIO_PIN)
    sleep(1)

