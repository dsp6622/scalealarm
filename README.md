# ScaleAlarm

(Stupid) Simple python implementation of an alarm clock for embedded linux that requires keeping a scale 
active. for a specified amount of time in order to turn off the alarm. The concept is similar to that of 
Ruggie.co. The not included relax.wav plays while standing on the scale (or activating GPIO pin 40, 
active low), alarm.wav plays otherwise. Once relax.wav ends, the alarm has been turned off. alarm.wav plays 
five times by default otherwise.
 


Dependencies: 
  python3
  pygame - For some reason most audio libraries have issues on the pi... pygame worked
           pyaudio was what I originally used, but I had issues setting it up the first
		   time and couldn't get it to work this time around.
  .wav files with known bitrates (44100 is what's in the source file)
  cron to actually run the alarm. No need to re-invent the wheel


This was actually written pre-ruggie release for a Raspberry Pi 3, as I didn't want to wait a year for 
shipping at the time. Unfortunately, my pi didn't survive shipping during a move, so rather than ordering 
a new pi I ordered a Ruggie. A year or two later, my Ruggie stopped functioning, and I've returned to 
being unable to wake up easily in the morning. Ruggie was definitely not worth the price of ~$80 at the time 
(or $70 as of now). Considering that price, I expected quality/longevity and non-volatile configuration at 
the least, neither of which were present. It's become a problem again, so I've decided to resurrect
the simple project and throw it in source control. My priority is having an alarm clock that works for me. 
As such, the quality of the code will reflect functionality over maintainability and portability.

I used cron to perform the actual alarm scheduling, because it exists, so why not.

You will need to provide the actual .wav files alarm.wav and relax.wav in the same directory as the
python source file. Hard coded file names/paths are a great example of corners being cut :P 

Replace pyalarm in the crontab with the full path to wherever you put the source file, or if you
really like doing things weird, add a symlink /bin/pyalarm.py -> path_to_source or something


You will have to wire things up to support whatever hardware configuration you're using. When I was working
with a bathroom scale, I found a line that outputted 3.3v, and hooked that to GPIO board pin 40 with
a 2k resistor in series and a 30k pull down resistor. I also tied the pi's ground pin to the scale ground 
so the 3.3v would have the correct reference. This time around I'm re-using the Ruggie sensor, since I had 
an issue with the scale randomly turning on. I even tried using an opto-isolator at one point to make sure 
the pi wasn't causing the scale to misbehave, so I suspect RF interference from the wires connected to the 
scale power planes feeding into the sensor amplifiers. The Ruggie switch appears to be high impedance normally, 
and low impedance (about 5 ohms) when activated. I'll be using a 10k resistor in series with the Ruggie sensor
from pi 3.3V to ground, connecting the center to GPIO pin 40. This will protect the GPIO pins and also prevent
excessive current draw. 

Bathroom scale based:
	Resistors were chosen without much thought, limiting current to the GPIO pin while also not pulling the voltage down too hard. 
	The bottom resistor value can be much larger, it was just what I had laying around at the time. The top resistor can also be larger
	in that case, but I wouldn't go any smaller than 2k.
	Alternatively, if your scale outputs 5V, resistor values should be chosen to drop to 3.3V (18k and 33k should be close enough)


  Scale activated 3.3V
    ___
     │
     │
  2k Ω
     │
     ├─── GPIO 40
     │
 30k Ω
     │
    ─┴─  (Pi ground and Scale ground tied together)
     -
 


Ruggie based:

  Pi 3.3V
    ___
     │
     │
 10k Ω
     │
     ├─── GPIO 40
     │
     └─┐
       »  Normally open switch
     ┌─┘
     │
    ─┴─
     -
 
